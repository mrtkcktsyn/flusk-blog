from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, g
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps


# User registration form
class RegisterationForm(Form):
    name = StringField("Fullname", validators=[validators.length(min = 4, max = 25)])
    email = StringField("Email", validators=[validators.Email(message = "Please enter a valid adress")])
    username = StringField("Username", validators=[validators.length(min = 5, max = 35)])
    password = PasswordField("Password", validators = [
        validators.DataRequired(message = "Please assign a password"),
        validators.EqualTo(fieldname = "confirm", message = "Your password does not match")    
    ])
    confirm = PasswordField("Confirm Password")

# Login Form
class LoginForm(Form):
    username = StringField("Username")
    password = PasswordField("Password")


# Article Form
class ArticleForm(Form):
    title = StringField("Title", validators=[validators.Length(min=5, max=100)])
    content = TextAreaField("Content", validators=[validators.Length(min=5, max=50000)])

app = Flask(__name__)

app.secret_key = "csblog"

app.config["MYSQL_HOST"] = "localhost"
app.config["MYSQL_USER"] = "root"
app.config["MYSQL_PASSWORD"] = "1429"
app.config["MYSQL_DB"] = "csblog"
app.config["MYSQL_CURSORCLASS"] = "DictCursor"


mysql = MySQL(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/about")
def about():
    return render_template("about.html")

# Articles Page
@app.route("/articles")
def articles():
    cursor = mysql.connection.cursor()
    query = "Select * from articles"
    result = cursor.execute(query)

    if result > 0:
        articles = cursor.fetchall()
        return render_template("articles.html", articles = articles)
    else:
        return render_template("articles.html")

# Login Decorator
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if "logged_in" in session:
            return f(*args, **kwargs)
        else:
            flash("Please login to view this page.", "warning")
            return redirect(url_for("login"))
    return decorated_function


# Dashboard Page
@app.route("/dashboard")
@login_required
def dashboard():
    cursor = mysql.connection.cursor()
    query = "Select * from articles where author = %s"
    result = cursor.execute(query, (session["username"],))
    if result > 0:
        articles = cursor.fetchall()
        return render_template("dashboard.html", articles = articles)
    else:
        return render_template("dashboard.html")

# Register Process
@app.route("/register", methods = ["GET", "POST"])
def register():
    form = RegisterationForm(request.form)

    if request.method == "POST" and form.validate():
        name = form.name.data
        username = form.username.data
        email = form.email.data
        password = sha256_crypt.encrypt(form.password.data)

        cursor = mysql.connection.cursor()

        query = "Insert into users(name,email,username,password) VALUES(%s, %s, %s, %s)"

        cursor.execute(query, (name, email, username, password))
        mysql.connection.commit()

        cursor.close()

        flash("Register Succeed!", category="success")

        return redirect(url_for("login"))
    else:
        return render_template("register.html", form = form)
        

#Login Process
@app.route("/login", methods = ["GET", "POST"])
def login():
    form = LoginForm(request.form)

    if request.method == "POST":
        username = form.username.data
        password_entered = form.password.data

        cursor = mysql.connection.cursor()

        query = "Select * from users where username = %s"

        result = cursor.execute(query, (username,))

        if result > 0:
            data = cursor.fetchone()
            real_password = data["password"]
            if sha256_crypt.verify(password_entered, real_password):
                flash("Login Succeed!", "success")

                session["logged_in"] = True
                session["username"] = username

                return redirect(url_for("index"))
            else:
                flash("Wrong Password.", "danger")
                return redirect(url_for("login"))
        else:
            flash("Username doesn't exist...", "danger")
            return redirect(url_for("login"))

    return render_template("login.html", form = form)


# Logout Process
@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("index"))


# Article Details (Selected from Dashboard)
@app.route("/article/<string:id>")
def article(id):
    cursor = mysql.connection.cursor()
    query = "Select * from articles where id = %s"
    result = cursor.execute(query, (id,))

    if result > 0:
        article = cursor.fetchone()
        return render_template("article.html", article = article)
    else:
        return render_template("article.html")



# Adding Article
@app.route("/addarticle", methods = ["GET", "POST"])
def addarticle():
    form = ArticleForm(request.form)
    if request.method == "POST" and form.validate():
        title = form.title.data
        content = form.content.data

        cursor = mysql.connection.cursor()
        query = "Insert into articles(title, author, content) VALUES(%s, %s, %s)"
        cursor.execute(query, (title, session["username"], content))
        mysql.connection.commit()
        cursor.close()
        flash("The article has been added successfully.", "success")

        return redirect(url_for("dashboard"))

    return render_template("addarticle.html", form = form)


# Deleting Article
@app.route("/delete/<string:id>")
@login_required
def delete(id):
    cursor = mysql.connection.cursor()
    query = "Select * from articles where author = %s and id = %s"
    result = cursor.execute(query, (session["username"], id))

    if result > 0:
        deleting_query = "Delete from articles where id = %s"
        cursor.execute(deleting_query, (id,))
        mysql.connection.commit()
        return redirect(url_for("dashboard"))
    else:
        flash("There is no article or you have no permission.", "danger")
        return redirect(url_for("index"))


# Updating Article
@app.route("/update/<string:id>", methods = ["GET", "POST"])
@login_required
def update(id):
    if request.method == "GET":
        cursor = mysql.connection.cursor()
        query = "Select * from articles where id = %s and author = %s"
        result = cursor.execute(query, (id, session["username"]))
        if result == 0:
            flash("There is no article or you have no permission for this process", "danger")
            return redirect(url_for("index"))
        else:
            article = cursor.fetchone()
            form = ArticleForm()
            form.title.data = article["title"]
            form.content.data = article["content"]
            return render_template("update.html", form = form)
    else:
        # POST Request
        form = ArticleForm(request.form)
        new_title = form.title.data
        new_content = form.content.data
        update_query = "Update articles Set title = %s, content = %s where id = %s"
        cursor = mysql.connection.cursor()
        cursor.execute(update_query, (new_title, new_content, id))
        mysql.connection.commit()
        flash("Updated Successfully.", "success")

        return redirect(url_for("dashboard"))


# Search URL
@app.route("/search", methods=["GET", "POST"])
def search():
    if request.method == "GET":
        return redirect(url_for("index"))
    else:
        keyword = request.form.get("keyword")
        cursor = mysql.connection.cursor()
        query = "Select * from articles where title like '%" + keyword + "%' "
        result = cursor.execute(query)
        if result == 0:
            flash("Couldn't find any article.", "warning")
            return redirect(url_for("articles"))
        else:
            articles = cursor.fetchall()
            return render_template("articles.html", articles = articles)
        



if __name__ == "__main__":
    app.run(debug=True)


